/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanakit.finalproject;

import java.io.Serializable;

/**
 *
 * @author COOL
 */
public class Item implements Serializable {
    private String ProductCode;
    private String Item;
    private int Price;
    private int Balance;

    @Override
    public String toString() {
        return "Data{" + "Product=" + ProductCode + ", Item=" + Item + ", Price=" + Price + ", Balance=" + Balance + '}';
    }

    public String getProduct() {
        return ProductCode;
    }

    public void setProduct(String Product) {
        this.ProductCode = Product;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String Item) {
        this.Item = Item;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getBalance() {
        return Balance;
    }

    public void setBalance(int Balance) {
        this.Balance = Balance;
    }

    public Item(String Product, String Item, int Price, int Balance) {
        this.ProductCode = Product;
        this.Item = Item;
        this.Price = Price;
        this.Balance = Balance;
    }
    
}
